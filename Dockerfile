from  truevolve/tvgobuilder:latest

USER root
RUN apt-get install -y g++-mingw-w64-i686-posix g++-mingw-w64-x86-64-posix gcc-mingw-w64-i686-posix gcc-mingw-w64-x86-64-posix swig gcc-multilib wget && \
    wget https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu/12.2.rel1/binrel/arm-gnu-toolchain-12.2.rel1-x86_64-aarch64-none-linux-gnu.tar.xz && \
    tar -xaf arm-gnu-toolchain-12.2.rel1-x86_64-aarch64-none-linux-gnu.tar.xz -C /usr && \
    rm arm-gnu-toolchain-12.2.rel1-x86_64-aarch64-none-linux-gnu.tar.xz

copy debian /usr/debian

USER developer

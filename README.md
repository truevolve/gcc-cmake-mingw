# To build

```bash
docker-compose build
version=20.2
docker tag truevolve/gcc-cmake-mingw:latest truevolve/gcc-cmake-mingw:$version
docker push -a truevolve/gcc-cmake-mingw
```

